package com.company;


public class Main {


    public static void main(String[] args) {
        FastReaderPresenter presenter = new FastReaderPresenter();
        boolean flag = true;

        while (flag) {
            System.out.println("Доброго времени суток, Вас приветствует консольный калькулятор\n Выберите операцию +,-,/,* и нажмите Enter, для выхода введите слово exit");
            String operation = presenter.nextLine();
            if (operation.equals("exit")) {
                flag = false;
            } else {

                System.out.println("Введите первое значение и нажмите Enter:");
                int firstNumber = presenter.nextInt();
                System.out.println("Введите второе значение и нажмите Enter:");
                int secondNumber = presenter.nextInt();


                switch (operation) {
                    case "+":
                        presenter.sum(firstNumber, secondNumber);
                        break;
                    case "-":
                        presenter.subtraction(firstNumber, secondNumber);
                        break;
                    case "/":
                        presenter.division(firstNumber, secondNumber);
                        break;
                    case "*":
                        presenter.multiplication(firstNumber, secondNumber);
                        break;
                    default:
                        System.out.println("Такую операцию не обрабатываем");
                }
            }

        }


    }
}
