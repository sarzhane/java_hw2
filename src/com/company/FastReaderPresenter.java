package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

 class FastReaderPresenter {
    BufferedReader br;
    StringTokenizer st;

    public FastReaderPresenter() {
        br = new BufferedReader(new
                InputStreamReader(System.in));
    }

    String next() {
        while (st == null || !st.hasMoreElements()) {
            try {
                st = new StringTokenizer(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return st.nextToken();
    }

    int nextInt() {
        return Integer.parseInt(next());
    }

    long nextLong() {
        return Long.parseLong(next());
    }

    double nextDouble() {
        return Double.parseDouble(next());
    }

    String nextLine() {
        String str = "";
        try {
            str = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
     public void sum(int a, int b) {
         int result = a + b;
         System.out.println("Результат: " + result);
     }

     public void subtraction(int a, int b) {
         int result = a - b;
         System.out.println("Результат: " + result);
     }

     public void division(int a, int b) {
         int result = a / b;
         System.out.println("Результат: " + result);
     }

     public void multiplication(int a, int b) {
         int result = a * b;
         System.out.println("Результат: " + result);
     }
}
